package net.iessanclemente.a19lazaropm.aservice.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import net.iessanclemente.a19lazaropm.aservice.ui.secondary.FichaMantenimientoActivity;
import net.iessanclemente.a19lazaropm.aservice.ui.forms.FormNewMantenimientoActivity;
import net.iessanclemente.a19lazaropm.aservice.R;
import net.iessanclemente.a19lazaropm.aservice.security.SecurityCipherWithKey;
import net.iessanclemente.a19lazaropm.aservice.database.dao.DataBaseOperations;
import net.iessanclemente.a19lazaropm.aservice.database.dto.Contacto;
import net.iessanclemente.a19lazaropm.aservice.database.dto.Empresa;
import net.iessanclemente.a19lazaropm.aservice.database.dto.Fabricante;
import net.iessanclemente.a19lazaropm.aservice.database.dto.Mantenimiento;
import net.iessanclemente.a19lazaropm.aservice.database.dto.Tecnico;
import net.iessanclemente.a19lazaropm.aservice.database.dto.TipoLongFlow;
import net.iessanclemente.a19lazaropm.aservice.database.dto.TipoVitrina;
import net.iessanclemente.a19lazaropm.aservice.database.dto.Vitrina;
import net.iessanclemente.a19lazaropm.aservice.reports.TemplatePdf;
import net.iessanclemente.a19lazaropm.aservice.reports.ViewPdfActivity;
import net.iessanclemente.a19lazaropm.aservice.utils.UtilsMenu;

import java.util.List;
import java.util.Locale;

public class ListMantenimientosAdapter extends RecyclerView.Adapter<ListMantenimientosAdapter.ViewHolder> {

    public static final String SUPERVISOR = "BBR/b9r6ZEsNFF7tdbo+/A==";
    private final String KEY_ATLAS_ROMERO = "AtlasRomero";
    private final LayoutInflater listElementsMantenimientosInflater;
    private final Context context;
    private final ActivityResultLauncher<Intent> activityResultLauncher;
    private final ProgressBar createReportProgressBar;
    private List<ElementListMantenimientos> listElementsMantenimientos;
    private TemplatePdf templatePdf;

    public ListMantenimientosAdapter(
            List<ElementListMantenimientos> listElementsMantenimientos,
            ActivityResultLauncher<Intent> activityResultLauncher,
            ProgressBar createReportProgressBar,
            Context context) {
        this.listElementsMantenimientosInflater = LayoutInflater.from(context);
        this.context = context;
        this.activityResultLauncher = activityResultLauncher;
        this.listElementsMantenimientos = listElementsMantenimientos;
        this.createReportProgressBar = createReportProgressBar;
        createReportProgressBar.setVisibility(View.GONE);
    }

    @NonNull
    @Override
    public ListMantenimientosAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = listElementsMantenimientosInflater.inflate(
                R.layout.element_list_mantenimientos, parent, false);
        return new ListMantenimientosAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListMantenimientosAdapter.ViewHolder holder, int position) {
        holder.bindData(listElementsMantenimientos.get(position));

        //Manejo el evento onClick sobre el CardView del Mantenimiento
        holder.mantenimientoCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = holder.getAdapterPosition();
                //paso a la actividad de la ficha de la vitrina
                Intent intent = new Intent(context, FichaMantenimientoActivity.class);
                intent.putExtra("NOMBRE_EMPRESA", listElementsMantenimientos.get(pos).getEmpresa());
                intent.putExtra("ID_VITRINA", listElementsMantenimientos.get(pos).getIdVitrina());
                intent.putExtra("ID_MANTENIMIENTO", listElementsMantenimientos.get(pos).getId());
                context.startActivity(intent);
            }
        });

        holder.mantenimientoCardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                PopupMenu popupMenu = new PopupMenu(context, v);
                UtilsMenu.setForceShowIcon(popupMenu);
                popupMenu.inflate(R.menu.menu_contextual_mantenimientos);
                popupMenu.show();

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int pos = holder.getAdapterPosition();
                        DataBaseOperations datos = DataBaseOperations.getInstance(context);
                        Mantenimiento mantenimiento = datos.selectMantenimientoWithId(
                                listElementsMantenimientos.get(pos).getId());

                        if (mantenimiento != null) {
                            int idMantenimiento = mantenimiento.getId();

                            switch (item.getItemId()) {
                                case R.id.menuItemDelete:
                                    deleteMaintenance(pos, datos, idMantenimiento);
                                    break;
                                case R.id.menuItemUpdate:
                                    showSupervisorPinDialog(pos);
                                    break;
                                case R.id.menuViewReport:
                                    Thread hiloCreateAndView = new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            createReportPdf(pos, datos, idMantenimiento);
                                            Intent intentViewPDF = new Intent(
                                                    context, ViewPdfActivity.class);
                                            intentViewPDF.putExtra(
                                                    "PATH",
                                                    templatePdf.getPdfFile().getAbsolutePath());
                                            activityResultLauncher.launch(intentViewPDF);
                                        }
                                    });
                                    hiloCreateAndView.start();
                                    createReportProgressBar.setVisibility(View.VISIBLE);
                                    break;
                                case R.id.menuSendReport:
                                    if (templatePdf == null) {
                                        Thread hiloCreate = new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                createReportPdf(pos, datos, idMantenimiento);
                                                final String[] addresses = {
                                                        "sat@atlasromero.com",
                                                        "administracion@atlasromero.com",
                                                        "laguipemo@me.com"};
                                                final String subject = "Informe del mantenimiento";
                                                String textoEmail = "Le adjuntamos el informe del " +
                                                        "mantenimiento realizado a su vitrina";
                                                Intent intentEmail = new Intent(Intent.ACTION_SENDTO);
                                                intentEmail.setData(Uri.parse("mailto:"));
                                                intentEmail.putExtra(Intent.EXTRA_EMAIL, addresses);
                                                intentEmail.putExtra(Intent.EXTRA_SUBJECT, subject);
                                                intentEmail.putExtra(Intent.EXTRA_TEXT, textoEmail);
                                                Uri uriPdf = Uri.fromFile(templatePdf.getPdfFile());
                                                intentEmail.putExtra(Intent.EXTRA_STREAM, uriPdf);
                                                if (intentEmail.resolveActivity(context.getPackageManager()) != null) {
                                                    activityResultLauncher.launch(intentEmail);
                                                }
                                            }
                                        });
                                        hiloCreate.start();
                                        createReportProgressBar.setVisibility(View.VISIBLE);
                                    } else {
                                        templatePdf.attachPdfAndSendMail();
                                    }
                                    break;
                            }
                        }
                        return false;
                    }
                });
                return true;
            }
        });
    }

    private void createReportPdf(int pos, DataBaseOperations datos, int idMantenimiento) {
        Empresa empresa = datos.selectEmpresaWithName(
                listElementsMantenimientos.get(pos).getEmpresa());
        Contacto contacto = datos.selectContactoWithId(empresa.getIdContacto());
        Mantenimiento mantenimiento = datos.selectMantenimientoWithId(idMantenimiento);
        Tecnico tecnico = datos.selectTecnicoWithId(mantenimiento.getIdTecnico());
        Vitrina vitrina = datos.selectVitrinaWithId(mantenimiento.getIdVitrina());
        Fabricante fabricante = datos.selectFabricanteWithId(vitrina.getIdFabricante());
        String vitrinaReferencia = vitrina.getVitrinaReferencia();
        String vitrinaInventario = vitrina.getVitrinaInventario();
        TipoVitrina tipoVitrina = datos.selectTipoVitrinaWithId(vitrina.getIdTipo());
        int longitudVitrina = datos.getLongitudVitrinaWithIdLongitud(vitrina.getIdLongitud());
        float longitudGullotina = datos.getGuillotinaWithIdLongitud(vitrina.getIdLongitud());
        TipoLongFlow tipoLongFlow = datos.selectTipoLongFlowWithId(vitrina.getIdTipo(), vitrina.getIdLongitud());
        //Creo instancia del template para nuestro documento pdf
        templatePdf = new TemplatePdf(context);
        //abro el documento
        templatePdf.openDocument();
        //añado metadatos
        templatePdf.addMetaData(
                "Revisión de vitrina de gases",
                "Mantenimiento", tecnico.getTecnicoNombre());
        //añado los títulos
        String serialNum;
        if (vitrinaReferencia != null && !vitrinaReferencia.isEmpty() &&
                !vitrinaReferencia.equals("-")) {
            serialNum = vitrinaReferencia;
        } else if (vitrinaInventario != null && !vitrinaInventario.isEmpty() &&
                !vitrinaInventario.equals("-")) {
            serialNum = vitrinaInventario;
        } else {
            serialNum = "-";
        }
        templatePdf.addTitles("REVISION DE VITRINA DE GASES", serialNum);
        //Añado datos de la empresa
        templatePdf.addDatosEmpresa(
                mantenimiento.getFecha(),
                empresa.getEmpresaNombre(),
                empresa.getEmpresaDirecc(),
                contacto.getContactoNombre(),
                contacto.getContactoTelef(),
                contacto.getContactoCorreo());
        //Añado datos de la vitrina
        templatePdf.addDatosVitrina(
                fabricante.getNombre(),
                tipoVitrina.getTipoVitrina(),
                vitrinaReferencia,
                String.valueOf(vitrina.getVitrinaAnho()),
                "-",
                vitrinaInventario,
                mantenimiento.isPuestaMarcha(),
                vitrina.getVitrinaContrato(),
                tecnico.getTecnicoNombre());
        //Añado nueva página
        templatePdf.addNewPage();
        //añado título comprobación y ajuste dle sistema
        templatePdf.addComprobAjusteSistemaExtracion(
                datos.getCualitativoWithId(mantenimiento.getFunCtrlDigi()),
                datos.getCualitativoWithId(mantenimiento.getVisSistExtr()),
                mantenimiento.isSegunDin(),
                mantenimiento.isSegunEn(),
                longitudVitrina,
                longitudGullotina);
        //añado tabla de mediciones
        float[] mediciones = datos.selectMedicionWithId(mantenimiento.getIdMedicion()).getAllValores();
        templatePdf.createTableMediciones(mediciones);
        //añado cálculo del volumen de extracción
        templatePdf.creatTableCalcVolExtraccion(
                longitudGullotina,
                mediciones[0],
                tipoLongFlow.getFlowMin(),
                tipoLongFlow.getFlowMax(),
                tipoLongFlow.getFlowRecom());

        //Añado nueva página
        templatePdf.addNewPage();

        //Añado las comprobaciones y ajustes de componentes y medios (grifos/monoreductores)
        templatePdf.addComprobAjusteComponentesFijos(
                datos.getCualitativoWithId(mantenimiento.getProtSuperf()),
                datos.getCualitativoWithId(mantenimiento.getJuntas()),
                datos.getCualitativoWithId(mantenimiento.getFijacion()),
                datos.getCualitativoWithId(mantenimiento.getFuncGuillo()),
                datos.getCualitativoWithId(mantenimiento.getEstadoGuillo()),
                mantenimiento.getValFuerzaGuillo(),
                datos.getCualitativoWithId(mantenimiento.getFuerzaGuillo()),
                datos.getCualitativoWithId(mantenimiento.getCtrlPresencia()),
                datos.getCualitativoWithId(mantenimiento.getAutoproteccion()),
                datos.getCualitativoWithId(mantenimiento.getGrifosMonored()));

        //Añado comentarios y evaluación final
        templatePdf.addComentAndFinalEvalu(
                mantenimiento.getComentario(),
                mantenimiento.isAcordeNormasReguSi(),
                mantenimiento.isNecesarioRepaSi());

        //Añado el pie de firmas
        templatePdf.addSignatures();

        //cierro el documento
        templatePdf.closeDocument();

        //Abro el pdf para estampar la numeración de páginas.
        templatePdf = new TemplatePdf(context);
        templatePdf.openDocumetStamperPageNumering();
    }

    private void deleteMaintenance(int pos, DataBaseOperations datos, int idMantenimiento) {
        boolean isMantenimientoDeleted = datos.deleteMantenimiento(idMantenimiento);
        if (isMantenimientoDeleted) {
            listElementsMantenimientos.remove(pos);
            notifyDataSetChanged();
            Toast.makeText(
                    context,
                    "Borrado Mantenimiento",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void showSupervisorPinDialog(int pos) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context, R.style.CustomAlertDialog);
        LayoutInflater customDialogInflater = LayoutInflater.from(context);
        View customDialogView = customDialogInflater.inflate(
                R.layout.alert_dialog_admin_permissions, null);
        EditText pinSupervisorEditText = customDialogView.findViewById(
                R.id.supervisorPinEditText);

        SecurityCipherWithKey security = new SecurityCipherWithKey();
        security.addKey(KEY_ATLAS_ROMERO);

        alertDialogBuilder.setView(customDialogView)
                .setTitle(R.string.pin)
                .setIcon(R.drawable.ic_alert)
                .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String pinEncrypted = security.encrypt(
                                pinSupervisorEditText.getText().toString().trim());
                        if(pinEncrypted.trim().equals(SUPERVISOR)) {
                            lunchMaintenanceEdition(pos);
                        } else {
                            Toast.makeText(context, "Pin incorrecto", Toast.LENGTH_SHORT).show();
                        }
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(
                                context,
                                "Cancelada la actualización",
                                Toast.LENGTH_SHORT).show();
                        dialog.cancel();
                    }
                });
        AlertDialog supervisorPinDialog = alertDialogBuilder.create();
        supervisorPinDialog.show();
    }

    private void lunchMaintenanceEdition(int pos) {
        //paso a la actividad de la ficha para un nuevo mantenimiento
        Intent intent = new Intent(
                context, FormNewMantenimientoActivity.class);
        intent.putExtra("TASK", "UPDATE");
        intent.putExtra(
                "NOMBRE_EMPRESA",
                listElementsMantenimientos.get(pos).getEmpresa());
        intent.putExtra(
                "ID_VITRINA",
                listElementsMantenimientos.get(pos).getIdVitrina());
        intent.putExtra(
                "ID_MANTENIMIENTO",
                listElementsMantenimientos.get(pos).getId());
        activityResultLauncher.launch(intent);
    }

    @Override
    public int getItemCount() {
        return listElementsMantenimientos.size();
    }

    public void addItems(List<ElementListMantenimientos> items) {
        this.listElementsMantenimientos = items;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final CardView mantenimientoCardView;
        private final TextView fechaMantenimiento;
        private final TextView valorVolumenExtraccionReal;
        private final TextView comentario;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mantenimientoCardView = itemView.findViewById(R.id.mantenimientoCardView);
            fechaMantenimiento = itemView.findViewById(R.id.fechaMantenimiTextView);
            valorVolumenExtraccionReal = itemView.findViewById(R.id.valorVolumenExtraccionRealTextView);
            comentario = itemView.findViewById(R.id.comentarioTextView);
        }

        public void bindData(final ElementListMantenimientos item) {
            fechaMantenimiento.setText(item.getFecha());
            valorVolumenExtraccionReal.setText(
                    String.format(Locale.getDefault(), "%.2f", item.getVolumenExtraccion()));
            comentario.setText(item.getComentario());
        }
    }
}

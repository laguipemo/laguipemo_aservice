package net.iessanclemente.a19lazaropm.aservice.reports;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.content.res.AppCompatResources;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.TabSettings;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;

import net.iessanclemente.a19lazaropm.aservice.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

public class TemplatePdf {
    private final Font FONT_TITLE = new Font(Font.FontFamily.HELVETICA, 20, Font.BOLD);
    private final Font FONT_SUBTITLE = new Font(Font.FontFamily.HELVETICA, 18, Font.BOLD);
    private final Font FONT_TEXT_BOLD_15 = new Font(Font.FontFamily.HELVETICA, 15, Font.BOLD);
    private final Font FONT_TEXT = new Font(Font.FontFamily.HELVETICA, 12);
    private final Font FONT_TEXT_ITALIC = new Font(Font.FontFamily.HELVETICA, 12, Font.ITALIC);
    private final Font FONT_TEXT_BOLD = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
    private final Font FONT_TEXT_BOLDITALIC = new Font(
            Font.FontFamily.HELVETICA, 12, Font.BOLDITALIC);
    private final Font FONT_TEXT_LITTLE = new Font(Font.FontFamily.HELVETICA, 10);
    private final Font FONT_TEXT_LITTLE_ITALIC = new Font(Font.FontFamily.HELVETICA, 10, Font.ITALIC);
    private final Font FONT_TEXT_LITTLE_BOLD = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
    private final Font FONT_TEXT_LITTLE_BOLDITALIC = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLDITALIC);
    private final Font FONT_TEXT_BOLD_RED = new Font(
            Font.FontFamily.HELVETICA, 12 , Font.BOLD, BaseColor.RED);
    private final Font FONT_TEXT_BOLD_BLUE = new Font(
            Font.FontFamily.HELVETICA, 12 , Font.BOLD, BaseColor.BLUE);

    private final Context context;
    private final File pdfsFolder;

    private File pdfFile;
    private Document document;
    private PdfWriter pdfWriter;

    private Paragraph paragraph;


    private class PageBackgroundAtlas extends PdfPageEventHelper {

        @Override
        public void onStartPage(PdfWriter writer, Document document) {
            addImageDrawableAsBackgroud(R.drawable.fondo_informe_atlas);
        }

    }

    //Constructor que necesita el contexto
    public TemplatePdf(Context context) {
        this.context = context;
        pdfsFolder = new File(context.getExternalFilesDir(Environment.DIRECTORY_DCIM), "pdfs");
    }

    //función con la que se crea el fichero físico en una carpeta previamente creada
    private void createFile(String fileName) {
        if (!pdfsFolder.exists()) {
            if(!pdfsFolder.mkdirs()) {
                Toast.makeText(
                        context, "No se pudo crear directorio", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        pdfFile = new File(pdfsFolder, fileName);
    }

    //función que crea y deja abierto el documento pdf vacío asociado al fichero físico creado.
    public void openDocument(String fileName) {
        //Creo el fichero físico en su carpeta
        createFile(fileName);
        try {
            //creo instancia de un documento con tamaño de pagina A4
            document = new Document(PageSize.A4);
            //creo la instancia del escritor que comunica al documento con el fichero físico por medio
            //del flujo de salida de tipo FileOutputStream
            pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(pdfFile));

            PageBackgroundAtlas pageBackgroundAtlas = new PageBackgroundAtlas();
            pdfWriter.setPageEvent(pageBackgroundAtlas);

            //abro el documento para ir añadiendole el contenido deseado
            document.open();
        } catch (Exception e) {
            Log.e("Error in openDocument: ", e.toString());
        }
    }

    //Create documento por defecto, con nombre TemplatePDF.pdf
    public void openDocument() {
        openDocument("InformeVitrina.pdf");
    }

    //Abrir documento existente a partir de su nombre para estamparle numeración de página
    public void openDocumetStamperPageNumering(String fileName) {
        //Creo el fichero físico en su carpeta
        createFile(fileName);
        try {
            PdfReader pdfReader = new PdfReader(new FileInputStream(pdfFile));
            PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileOutputStream(pdfFile));
            int totalPages = pdfReader.getNumberOfPages();
            for (int pagNum = 1; pagNum <= totalPages; pagNum++) {
                PdfContentByte underContentByte = pdfStamper.getUnderContent(pagNum);
                String pageNumering = String.format(
                        Locale.getDefault(), "Pagina %d de %d", pagNum, totalPages);
                BaseFont baseFont =BaseFont.createFont(
                        BaseFont.HELVETICA_OBLIQUE, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                float pagWith = pdfReader.getPageSize(pagNum).getWidth();
                float pagHight = pdfReader.getPageSize(pagNum).getHeight();
                underContentByte.saveState();
                underContentByte.beginText();
                underContentByte.setFontAndSize(baseFont, 12);
                underContentByte.setTextMatrix(pagWith/2 + 200,45);
                underContentByte.showText(pageNumering);
                underContentByte.endText();
                underContentByte.restoreState();
            }
            pdfStamper.close();
        } catch (IOException | DocumentException e) {
            Log.e("Error in openDocumentStamper", e.toString());
        }
    }

    //Abrir documento creado por defecto para estamparle numeración de página
    public void openDocumetStamperPageNumering() {
        openDocumetStamperPageNumering("InformeVitrina.pdf");
    }

    //función para cerrar el documento pdf
    public void closeDocument() {
        document.close();
    }

    public File getPdfFile() {
        return pdfFile;
    }

    //función para añadir los principales metadatos del fichero pdf
    public void addMetaData(String title, String subject, String author) {
        document.addTitle(title);
        document.addSubject(subject);
        document.addAuthor(author);
    }

    //función para adicionar título, subtítulo y fecha en el documento
    public void addTitles(String title, String serialNum) {
        //siempre se comienza creando el párrafo principal que será una variable de clase
        paragraph = new Paragraph();
        //añado los elementos como párrafos hijos (new Paragraph(String texto, Font font) al parrafo
        // principal. Para este ejemplo se han definido 3 fuentes
        addChildParagraphCenter(new Paragraph(title, FONT_TITLE), 0F, 10F);

        Paragraph paragraphTableSerial = new Paragraph();
        paragraphTableSerial.setSpacingBefore(20);
        PdfPTable pdfPTableSerial = new PdfPTable(1);
        pdfPTableSerial.setWidthPercentage(50);
        PdfPCell pdfPCellSerial = new PdfPCell(new Phrase(serialNum, FONT_TITLE));
        pdfPCellSerial.setFixedHeight(40);
        pdfPCellSerial.setBackgroundColor(BaseColor.LIGHT_GRAY);
        pdfPCellSerial.setHorizontalAlignment(Element.ALIGN_CENTER);
        pdfPCellSerial.setVerticalAlignment(Element.ALIGN_MIDDLE);
        pdfPTableSerial.addCell(pdfPCellSerial);

        paragraphTableSerial.add(pdfPTableSerial);
        addChildParagraphCenter(paragraphTableSerial, 10F, 10F);

        //defino un espacio al final del párrafo
        paragraph.setSpacingAfter(30);
        //siempre que se añade elemento (párrafo) al documento hay que capturar el posible error.
        try {
            document.add(paragraph);
        } catch (DocumentException e) {
            Log.e("Error in addTitles: ", e.toString());
        }
    }

    public void addDatosEmpresa(
            String date, String nameEmp, String direcEmp, String contactName, String contactTelef,
            String contactCorreo) {

        paragraph = new Paragraph();

        Paragraph lineaDate = new Paragraph();
        lineaDate.setSpacingBefore(10F);
        lineaDate.add(new Chunk("Fecha: ", FONT_TEXT_BOLD));
        lineaDate.add(new Chunk(date, FONT_TEXT));
        addChildParagraphLeft(lineaDate);

        Paragraph lineaCiente = new Paragraph();
        lineaCiente.setSpacingBefore(10F);
        lineaCiente.add(new Chunk("Cliente: ", FONT_TEXT_BOLD));
        lineaCiente.add(new Chunk(nameEmp, FONT_TEXT));
        addChildParagraphLeft(lineaCiente);

        Paragraph lineaDirec = new Paragraph();
        lineaDirec.setSpacingBefore(10F);
        lineaDirec.add(new Chunk("Dirección: ", FONT_TEXT_BOLD));
        lineaDirec.add(new Chunk(direcEmp, FONT_TEXT));
        addChildParagraphLeft(lineaDirec);

        Paragraph lineaContactName = new Paragraph();
        lineaContactName.setSpacingBefore(10F);
        lineaContactName.add(new Chunk("Persona de contacto: ", FONT_TEXT_BOLD));
        lineaContactName.add(new Chunk(contactName, FONT_TEXT));
        addChildParagraphLeft(lineaContactName);

        Paragraph lineaContactTelef = new Paragraph();
        lineaContactTelef.setSpacingBefore(10F);
        lineaContactTelef.add(new Chunk("Telefono: ", FONT_TEXT_BOLD));
        lineaContactTelef.add(new Chunk(contactTelef, FONT_TEXT));
        addChildParagraphLeft(lineaContactTelef);

        Paragraph lineaContactCorreo = new Paragraph();
        lineaContactCorreo.setSpacingBefore(10F);
        lineaContactCorreo.add(new Chunk("E-mail: ", FONT_TEXT_BOLD));
        lineaContactCorreo.add(new Chunk(contactCorreo, FONT_TEXT));
        addChildParagraphLeft(lineaContactCorreo);

        paragraph.setSpacingBefore(50F);
        paragraph.setSpacingAfter(80F);

        try {
            document.add(paragraph);
        } catch (DocumentException e) {
            Log.e("Error in addDatosEmpresa: ", e.toString());
        }
    }

    public void addDatosVitrina(
            String fabricante, String tipo, String referencia, String anho, String serialNum,
            String inventario, boolean puestaMarcha, String mantenimiento, String tecnicoName) {

        paragraph = new Paragraph();

        Paragraph lineaFabricante = new Paragraph();
        lineaFabricante.add(new Chunk("Fabricante: ", FONT_TEXT_BOLD));
        lineaFabricante.add(new Chunk(fabricante, FONT_TEXT));
        lineaFabricante.setSpacingBefore(10F);
        addChildParagraphLeft(lineaFabricante);

        Paragraph lineaTipo = new Paragraph();
        lineaTipo.add(new Chunk("Tipo: ", FONT_TEXT_BOLD));
        lineaTipo.add(new Chunk(tipo, FONT_TEXT));
        lineaTipo.setSpacingBefore(10F);
        addChildParagraphLeft(lineaTipo);

        Paragraph lineaReferencia = new Paragraph();
        lineaReferencia.add(new Chunk("Referencia: ", FONT_TEXT_BOLD));
        lineaReferencia.add(new Chunk(referencia, FONT_TEXT));
        lineaReferencia.setSpacingBefore(10F);
        addChildParagraphLeft(lineaReferencia);

        Paragraph lineaAnho = new Paragraph();
        lineaAnho.add(new Chunk("Año construcción: ", FONT_TEXT_BOLD));
        lineaAnho.add(new Chunk(anho, FONT_TEXT));
        lineaAnho.setSpacingBefore(10F);
        addChildParagraphLeft(lineaAnho);

        Paragraph lineaSerialNum = new Paragraph();
        lineaSerialNum.add(new Chunk("Nº de serie: ", FONT_TEXT_BOLD));
        lineaSerialNum.add(new Chunk(serialNum, FONT_TEXT));
        lineaSerialNum.setSpacingBefore(10F);
        addChildParagraphLeft(lineaSerialNum);

        Paragraph lineaInventario = new Paragraph();
        lineaInventario.add(new Chunk("Nº de inventario: ", FONT_TEXT_BOLD));
        lineaInventario.add(new Chunk(inventario, FONT_TEXT));
        lineaInventario.setSpacingBefore(10F);
        addChildParagraphLeft(lineaInventario);

        Paragraph lineaPuestaMarcha = new Paragraph();
        lineaPuestaMarcha.add(new Chunk("Puesta en marcha: ", FONT_TEXT_BOLD));
        lineaPuestaMarcha.add(new Chunk(puestaMarcha? "Si": "No", FONT_TEXT));
        lineaPuestaMarcha.setSpacingBefore(10F);
        addChildParagraphLeft(lineaPuestaMarcha);

        Paragraph lineaMantenimiento = new Paragraph();
        lineaMantenimiento.add(new Chunk("Servicio de mantenimiento: ", FONT_TEXT_BOLD));
        lineaMantenimiento.add(new Chunk(mantenimiento, FONT_TEXT));
        lineaMantenimiento.setSpacingBefore(10F);
        addChildParagraphLeft(lineaMantenimiento);

        Paragraph lineaTecnicoName = new Paragraph();
        lineaTecnicoName.add(new Chunk("Técnico responsable: ", FONT_TEXT_BOLD));
        lineaTecnicoName.add(new Chunk(tecnicoName, FONT_TEXT));
        lineaTecnicoName.setSpacingBefore(10F);
        addChildParagraphLeft(lineaTecnicoName);

        try {
            document.add(paragraph);
        } catch (DocumentException e) {
            Log.e("Error in addDatosVitrina: ", e.toString());
        }
    }

    public void addComprobAjusteSistemaExtracion(
            String funcCtrlDigi, String visSistExtr, boolean segunDin, boolean segunEn,
            int longitudVitrina, float longitudGuillotina) {

        paragraph = new Paragraph();

        Paragraph lineaSubtitle = new Paragraph();
        lineaSubtitle.add(
                new Chunk("Comprobación y ajuste del sistema de extracción", FONT_SUBTITLE));
        lineaSubtitle.setSpacingBefore(30F);
        lineaSubtitle.setSpacingAfter(20F);
        addChildParagraphCenter(lineaSubtitle);

        Paragraph lineaCtrlDigi = new Paragraph();
        lineaCtrlDigi.add(
                new Chunk("Funcionamiento del controlador digital: ", FONT_TEXT_BOLD));
        lineaCtrlDigi.add(new Chunk(funcCtrlDigi, FONT_TEXT));
        lineaCtrlDigi.setSpacingBefore(10F);
        addChildParagraphLeft(lineaCtrlDigi);

        Paragraph lineaVisSistExtr = new Paragraph();
        lineaVisSistExtr.add(
                new Chunk("Inspección visual del sistema de extracción: ", FONT_TEXT_BOLD));
        lineaVisSistExtr.add(new Chunk(visSistExtr, FONT_TEXT));
        lineaVisSistExtr.setSpacingBefore(10F);
        addChildParagraphLeft(lineaVisSistExtr);

        Paragraph lineaSegunDin = new Paragraph();
        lineaSegunDin.add(
                new Chunk("Requerimiento según norma DIN 12924: ", FONT_TEXT_BOLD));
        lineaSegunDin.add(new Chunk(segunDin? "Si": "No", FONT_TEXT));
        lineaSegunDin.setSpacingBefore(10F);
        addChildParagraphLeft(lineaSegunDin);

        Paragraph lineaSegunEn = new Paragraph();
        lineaSegunEn.add(
                new Chunk("Requerimiento según norma EN 14175: ", FONT_TEXT_BOLD));
        lineaSegunEn.add(new Chunk(segunEn? "Si": "No", FONT_TEXT));
        lineaSegunEn.setSpacingBefore(10F);
        addChildParagraphLeft(lineaSegunEn);

        Paragraph lineaSubtitleVolExtrac = new Paragraph();
        lineaSubtitleVolExtrac.add(
                new Chunk("Medición de volumen de extracción",
                        FONT_TEXT_BOLD_15));
        lineaSubtitleVolExtrac.setSpacingBefore(30F);
        lineaSubtitleVolExtrac.setSpacingAfter(10F);
        addChildParagraphLeft(lineaSubtitleVolExtrac);

        Paragraph lineaLongVitrina = new Paragraph();
        lineaLongVitrina.add(
                new Chunk("Dimensiones de la Vitrina de Gases: ", FONT_TEXT_BOLD));
        lineaLongVitrina.add(new Chunk(String.valueOf(longitudVitrina), FONT_TEXT));
        lineaLongVitrina.setSpacingBefore(10F);
        addChildParagraphLeft(lineaLongVitrina);

        Paragraph lineaLongGuillotina = new Paragraph();
        lineaLongGuillotina.add(
                new Chunk("Longitud de la Guillotina: ", FONT_TEXT_BOLD));
        lineaLongGuillotina.add(new Chunk(String.valueOf(longitudGuillotina), FONT_TEXT));
        lineaLongGuillotina.setSpacingBefore(10F);
        addChildParagraphLeft(lineaLongGuillotina);

        Paragraph lineaAlturaTrabajo = new Paragraph();
        lineaAlturaTrabajo.add(
                new Chunk("Altura de trabajo: ", FONT_TEXT_BOLD));
        lineaAlturaTrabajo.add(new Chunk("500", FONT_TEXT));
        lineaAlturaTrabajo.setSpacingBefore(10F);
        addChildParagraphLeft(lineaAlturaTrabajo);

        try {
            document.add(paragraph);
        } catch (DocumentException e) {
            Log.e("Error in addComprobAjusteSistemaExtracion: ", e.toString());
        }
    }

    //crear y añadir tabla mediciones
    public void createTableMediciones(float[] mediciones) {
        paragraph = new Paragraph();
        //Título de la tabla
        Paragraph lineaTitleMediciones = new Paragraph();
        lineaTitleMediciones.add(
                new Chunk("Velocidades en frontal (m/s)", FONT_TEXT_BOLD));
        lineaTitleMediciones.setSpacingBefore(30F);
        lineaTitleMediciones.setSpacingAfter(10F);
        addChildParagraphLeft(lineaTitleMediciones);

        //Definición de la tabla tipo de letra, numero de columnas y porcentange del ancho página
        paragraph.setFont(FONT_TEXT);
        PdfPTable pdfPTableMedi = new PdfPTable(mediciones.length - 1); //no incluye media
        pdfPTableMedi.setWidthPercentage(100);
        //contruir la cabecera de la tabla
        PdfPCell pdfPCell;
        for (int indexCol = 1; indexCol < mediciones.length; indexCol++)  {
            pdfPCell = new PdfPCell(new Phrase(String.valueOf(indexCol), FONT_TEXT_BOLD));
            pdfPCell.setFixedHeight(25);
            pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            pdfPCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            pdfPTableMedi.addCell(pdfPCell);
        }
        //añado las filas a la tabla (en este caso concreto hay una única fila
        int rowNumbers = 1;
        for (int indexRow = 0; indexRow < rowNumbers; indexRow++) {
            for (int indexCol = 1; indexCol < mediciones.length; indexCol++) {
                pdfPCell = new PdfPCell(
                        new Phrase(
                                String.format(Locale.getDefault(), "%.2f", mediciones[indexCol]),
                                FONT_TEXT));
                pdfPCell.setFixedHeight(40);
                pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                pdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                pdfPTableMedi.addCell(pdfPCell);
            }
        }
        paragraph.add(pdfPTableMedi);
        //Añado el valor medio de las mediciones justo debajo de la tabla (como "pie de tabla")
        Paragraph lineaMediaMediciones = new Paragraph();
        lineaMediaMediciones.add(
                new Chunk("Velocidad media (m/s): ", FONT_TEXT_BOLD));
        lineaMediaMediciones.add(
                new Chunk(String.format(Locale.getDefault(), "%.2f", mediciones[0]),
                        FONT_TEXT_BOLD));
        addChildParagraphLeft(lineaMediaMediciones);

        try {
            document.add(paragraph);
        } catch (DocumentException e) {
            Log.e("Error in createTableMediciones: ", e.toString());
        }
    }

    public void creatTableCalcVolExtraccion(
            float longitudGuillotina, float mediaMedicion, int volMin, int volMax, int volRecom) {
        paragraph = new Paragraph();
        //Título de la tabla
        Paragraph lineaTitleCalcVolExtr = new Paragraph();
        lineaTitleCalcVolExtr.add(
                new Chunk("Cálculo del volumen de extracción", FONT_TEXT_BOLD));
        lineaTitleCalcVolExtr.setSpacingBefore(30F);
        lineaTitleCalcVolExtr.setSpacingAfter(10F);
        addChildParagraphLeft(lineaTitleCalcVolExtr);

        Paragraph lineaCalcExpresion = new Paragraph();
        lineaCalcExpresion.add(
                new Chunk("Volumen de extracción (m³/h) = Longitud Gillotina (m) x " +
                        "Altura Trabajo (m) x Velocidad Aire (m/seg) x 3600 seg",
                        FONT_TEXT_LITTLE_ITALIC));
        lineaCalcExpresion.setSpacingAfter(10F);
        addChildParagraphLeft(lineaCalcExpresion);

        //Definición de la tabla tipo de letra, numero de columnas y porcentange del ancho página
        paragraph.setFont(FONT_TEXT);
        String[] headers = new String[] {
                "Longitud\nGillotina\n(m)",
                "Altura de\nTrabajo\n(m)",
                "Velocidad\ndel Aire\n(m/seg)",
                "Factor de\nconversión\n(seg)",
                "Volumen de\nextracción\n(m³/h)",
        };
        PdfPTable pdfPTableMedi = new PdfPTable(headers.length); //no incluye media
        pdfPTableMedi.setWidthPercentage(100);
        //contruir la cabecera de la tabla
        PdfPCell pdfPCell;
        for (int indexCol = 0; indexCol < headers.length; indexCol++)  {
            pdfPCell = new PdfPCell(new Phrase(headers[indexCol], FONT_TEXT_BOLD));
            pdfPCell.setFixedHeight(60);
            pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            pdfPCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            pdfPTableMedi.addCell(pdfPCell);
        }
        //calculo del volumen de extracción real
        float volExtr = longitudGuillotina * 0.5F * mediaMedicion * 3600F;
        //valores a añadir a la tabla en la única fila que tiene
        Phrase[] rowValues = new Phrase[] {
                new Phrase(String.format(Locale.getDefault(),"%.2f", longitudGuillotina), FONT_TEXT),
                new Phrase(String.format(Locale.getDefault(), "%.2f", 0.5F), FONT_TEXT),
                new Phrase(String.format(Locale.getDefault(), "%.2f", mediaMedicion), FONT_TEXT),
                new Phrase(String.format(Locale.getDefault(), "%d", 3600), FONT_TEXT),
                new Phrase(String.format(Locale.getDefault(), "%.2f", volExtr), FONT_TEXT_BOLD_15)
        };
        //añado los valores
        for (Phrase phrase: rowValues) {
            pdfPCell = new PdfPCell(phrase);
            pdfPCell.setFixedHeight(40);
            pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            pdfPTableMedi.addCell(pdfPCell);
        }

        //adiciono tabla al párrafo
        paragraph.add(pdfPTableMedi);

        //Añado el valor Minimo, Máximo y Recomenendado (como "pie de tabla")
        Paragraph lineaVolMinMaxRecom = new Paragraph();
        lineaVolMinMaxRecom.setTabSettings(new TabSettings(40F));

        lineaVolMinMaxRecom.add(
                new Chunk("Valores teóricos (m/s): ", FONT_TEXT_BOLD));
        lineaVolMinMaxRecom.add(Chunk.TABBING);
        lineaVolMinMaxRecom.add(
                new Chunk(String.format(
                        Locale.getDefault(), "Min = %d ", volMin), FONT_TEXT));
        lineaVolMinMaxRecom.add(Chunk.TABBING);
        lineaVolMinMaxRecom.add(
                new Chunk(String.format(
                        Locale.getDefault(), "Máx = %d ", volMax), FONT_TEXT));
        lineaVolMinMaxRecom.add(Chunk.TABBING);
        lineaVolMinMaxRecom.add(
                new Chunk(String.format(
                        Locale.getDefault(), "Recomendado = %d ", volRecom),
                        FONT_TEXT_BOLD));

        addChildParagraphLeft(lineaVolMinMaxRecom);

        try {
            document.add(paragraph);
        } catch (DocumentException e) {
            Log.e("Error in createTableCalcVolExtraccion: ", e.toString());
        }
    }

    public void addComprobAjusteComponentesFijos(
            String protSuperf, String juntas, String fijacion, String funcGuillo, String estadoGuillo,
            float valFuerzaGuillo, String fuerzaGuillo, String ctrlPresencia, String autoproteccion,
            String grifosMonored) {

        paragraph = new Paragraph();

        Paragraph lineaSubtitle = new Paragraph();
        lineaSubtitle.add(
                new Chunk("Comprobación y ajuste de componentes mecánicos y medios (grifos/manoreductores)",
                        FONT_SUBTITLE));
        lineaSubtitle.setSpacingBefore(30F);
        lineaSubtitle.setSpacingAfter(20F);
        addChildParagraphCenter(lineaSubtitle);

        Paragraph lineaProtSuperf = new Paragraph();
        lineaProtSuperf.add(
                new Chunk("Protección de la superficie: ", FONT_TEXT_BOLD));
        lineaProtSuperf.add(new Chunk(protSuperf, FONT_TEXT));
        lineaProtSuperf.setSpacingBefore(10F);
        addChildParagraphLeft(lineaProtSuperf);

        Paragraph lineaJuntas = new Paragraph();
        lineaJuntas.add(
                new Chunk("Aislamiento de las juntas en el interior: ", FONT_TEXT_BOLD));
        lineaJuntas.add(new Chunk(juntas, FONT_TEXT));
        lineaJuntas.setSpacingBefore(10F);
        addChildParagraphLeft(lineaJuntas);

        Paragraph lineaFijacion = new Paragraph();
        lineaFijacion.add(
                new Chunk("Sujeción correcta de las piezas fijas: ", FONT_TEXT_BOLD));
        lineaFijacion.add(new Chunk(fijacion, FONT_TEXT));
        lineaFijacion.setSpacingBefore(10F);
        addChildParagraphLeft(lineaFijacion);

        Paragraph lineaFuncGuillo = new Paragraph();
        lineaFuncGuillo.add(
                new Chunk("Funcionamiento de la guillotina: ", FONT_TEXT_BOLD));
        lineaFuncGuillo.add(new Chunk(funcGuillo, FONT_TEXT));
        lineaFuncGuillo.setSpacingBefore(10F);
        addChildParagraphLeft(lineaFuncGuillo);

        Paragraph lineaEstadoGuillo = new Paragraph();
        lineaEstadoGuillo.add(
                new Chunk("Estado general de la guillotina: ", FONT_TEXT_BOLD));
        lineaEstadoGuillo.add(new Chunk(estadoGuillo, FONT_TEXT));
        lineaEstadoGuillo.setSpacingBefore(10F);
        addChildParagraphLeft(lineaEstadoGuillo);

        Paragraph lineaFuerzaGuillo = new Paragraph();
        lineaFuerzaGuillo.setTabSettings(new TabSettings(20F));
        lineaFuerzaGuillo.add(
                new Chunk("Fuerza para el movimiento vertical de la guillotina: ",
                        FONT_TEXT_BOLD));
        lineaFuerzaGuillo.add(
                new Chunk(
                        String.format(Locale.getDefault(), "%.2f N ", valFuerzaGuillo), FONT_TEXT));
        lineaFuerzaGuillo.add(Chunk.TABBING);
        lineaFuerzaGuillo.add(new Chunk(fuerzaGuillo, FONT_TEXT));
        lineaFuerzaGuillo.setSpacingBefore(10F);
        addChildParagraphLeft(lineaFuerzaGuillo);

        Paragraph lineaCtrlPresencia = new Paragraph();
        lineaCtrlPresencia.add(
                new Chunk("Control de presencia: ", FONT_TEXT_BOLD));
        lineaCtrlPresencia.add(new Chunk(ctrlPresencia, FONT_TEXT));
        lineaCtrlPresencia.setSpacingBefore(10F);
        addChildParagraphLeft(lineaCtrlPresencia);

        Paragraph lineaAutoproteccion = new Paragraph();
        lineaAutoproteccion.add(
                new Chunk("Autoproteccion: ", FONT_TEXT_BOLD));
        lineaAutoproteccion.add(new Chunk(autoproteccion, FONT_TEXT));
        lineaAutoproteccion.setSpacingBefore(10F);
        addChildParagraphLeft(lineaAutoproteccion);

        Paragraph lineaGrifosMonored = new Paragraph();
        lineaGrifosMonored.add(
                new Chunk("Compobación del funcionamiento de grifos/monoreductores: ",
                        FONT_TEXT_BOLD));
        lineaGrifosMonored.add(new Chunk(grifosMonored, FONT_TEXT));
        lineaGrifosMonored.setSpacingBefore(10F);
        addChildParagraphLeft(lineaGrifosMonored);

        try {
            document.add(paragraph);
        } catch (DocumentException e) {
            Log.e("Error in addComprobAjusteComponentesFijos: ", e.toString());
        }
    }

    public void addComentAndFinalEvalu(
            String comentarios, boolean isAcordeNormasReguSi, boolean isNecesarioRepaSi) {

        paragraph = new Paragraph();

        Paragraph lineaLabelComentarios = new Paragraph();
        lineaLabelComentarios.add(new Chunk("Comentarios: ", FONT_TEXT_BOLD));
        lineaLabelComentarios.setSpacingBefore(20F);
        addChildParagraphLeft(lineaLabelComentarios);
        Paragraph lineaComentarios = new Paragraph();
        lineaComentarios.setIndentationLeft(30F);
        lineaComentarios.add(new Paragraph(comentarios, FONT_TEXT));
        addChildParagraphLeft(lineaComentarios);

        Paragraph lineaLabelFinalEvalu = new Paragraph();
        lineaLabelFinalEvalu.add(new Chunk("Resultado: ", FONT_TEXT_BOLD));
        lineaLabelFinalEvalu.setSpacingBefore(30F);
        addChildParagraphLeft(lineaLabelFinalEvalu);

        Paragraph lineaAcordeNormRegu = new Paragraph();
        lineaAcordeNormRegu.setIndentationLeft(30F);
        lineaAcordeNormRegu.add(
                new Chunk(
                        "El equipo está de acuerdo con las normas / regulaciones pertinentes: ",
                        FONT_TEXT));
        lineaAcordeNormRegu.add(new Chunk(isAcordeNormasReguSi? "Si": "No", FONT_TEXT));
        lineaAcordeNormRegu.setSpacingBefore(10F);
        addChildParagraphLeft(lineaAcordeNormRegu);

        Paragraph lineaNecesarioRepa = new Paragraph();
        lineaNecesarioRepa.setIndentationLeft(30F);
        lineaNecesarioRepa.add(
                new Chunk(
                        "Por cuestiones de seguridad es necesaria la intervención del equipo: ",
                        FONT_TEXT));
        lineaNecesarioRepa.add(new Chunk(isNecesarioRepaSi? "Si": "No", FONT_TEXT));
        lineaNecesarioRepa.setSpacingBefore(10F);
        addChildParagraphLeft(lineaNecesarioRepa);

        try {
            document.add(paragraph);
        } catch (DocumentException e) {
            Log.e("Error in addComentAndFinalEvalu: ", e.toString());
        }
    }

    public void addSignatures() {
        paragraph = new Paragraph();

        //Añado pie de firma
        Paragraph lineaSignatures = new Paragraph();
        lineaSignatures.setSpacingBefore(80F);
        lineaSignatures.setIndentationLeft(30F);
        lineaSignatures.setTabSettings(new TabSettings(300F));

        lineaSignatures.add(
                new Chunk("Firma Técnico", FONT_TEXT_BOLD));
        lineaSignatures.add(Chunk.TABBING);
        lineaSignatures.add(
                new Chunk("Firma de la propiedad", FONT_TEXT_BOLD));

        addChildParagraphLeft(lineaSignatures);

        try {
            document.add(paragraph);
        } catch (DocumentException e) {
            Log.e("Error in addSignatures: ", e.toString());
        }
    }

    //función para añadir parrafos hijos centrados en el parrafo principal
    public void addChildParagraphCenter(Paragraph childParagraph) {
        addChildParagraphCenter(childParagraph, 0F, 0F);
    }

    public void addChildParagraphCenter(Paragraph childParagraph, float before, float after) {
        //defino como quiero alinear el párrafo hijo dentro de su padre.
        childParagraph.setAlignment(Element.ALIGN_CENTER);
        paragraph.setSpacingBefore(before);
        //añado el párrafo hijo al párrafo principal con el alineamiento indicado
        paragraph.add(childParagraph);
        paragraph.setSpacingAfter(after);
    }

    public void addChildParagraphLeft(Paragraph childParagraph) {
        addChildParagraphLeft(childParagraph, 0F, 0F);
    }

    public void addChildParagraphLeft(Paragraph childParagraph, float before, float after) {
        childParagraph.setAlignment(Element.ALIGN_LEFT);
        paragraph.setSpacingBefore(before);
        paragraph.add(childParagraph);
        paragraph.setSpacingAfter(after);
    }

    public void addChildParagraphRight(Paragraph childParagraph) {
        addChildParagraphRight(childParagraph, 0F, 0F);
    }

    public void addChildParagraphRight(Paragraph childParagraph, float before, float after) {
        childParagraph.setAlignment(Element.ALIGN_RIGHT);
        paragraph.setSpacingBefore(before);
        paragraph.add(childParagraph);
        paragraph.setSpacingAfter(after);
    }

    public void addChildParagraphJustified(Paragraph childParagraph) {
        addChildParagraphJustified(childParagraph, 0F, 0F);
    }

    public void addChildParagraphJustified(Paragraph childParagraph, float before, float after) {
        childParagraph.setAlignment(Element.ALIGN_JUSTIFIED);
        paragraph.setSpacingBefore(before);
        paragraph.add(childParagraph);
        paragraph.setSpacingAfter(after);
    }

    public void addChildParagraphAligned(Paragraph childParagraph, int alignment) {
        addChildParagraphAligned(childParagraph, alignment, 0F, 0F);
    }

    public void addChildParagraphAligned(
            Paragraph childParagraph, int alignment, float before, float after) {
        paragraph.setSpacingBefore(before);
        switch (alignment) {
            case Element.ALIGN_CENTER:
                addChildParagraphCenter(childParagraph);
                break;
            case  Element.ALIGN_LEFT:
                addChildParagraphLeft(childParagraph);
                break;
            case Element.ALIGN_RIGHT:
                addChildParagraphRight(childParagraph);
                break;
            case Element.ALIGN_JUSTIFIED:
                addChildParagraphJustified(childParagraph);
                break;
            default:
                childParagraph.setAlignment(alignment);
                paragraph.add(childParagraph);
        }
        paragraph.setSpacingAfter(after);
    }

    public void addParagraph(String text, int spaceBefore, int spaceAfter) {
        paragraph = new Paragraph(text, FONT_TEXT);
        paragraph.setSpacingBefore(spaceBefore);
        paragraph.setSpacingAfter(spaceAfter);
        try {
            document.add(paragraph);
        } catch (DocumentException e) {
            Log.e("Error in addParagraph: ", e.toString());
        }
    }

    public void addParagraph(String text) {
        addParagraph(text, 5, 5);
    }

    //añadir imagen
    public void addImage(Image image) {
        try {
            document.add(image);
        } catch (DocumentException e) {
            Log.e("Error in addImage: ", e.toString());
        }
    }

    //Se utiliza una imagen, entregada como recurso drawable, para que colocarla como fondo de página
    public void addImageDrawableAsBackgroud(int drawableResource) {
        //Tomo la imagen desde la carpeta de recursos, creo bitmap con ella, lo comprimo, lo
        //convierto en un array de byte termino creando con él una instancia Imagen() lista para
        //ajustar su tamaño de la página y colocarla en el origen (0, 0) (vértice inferior izquierdo)
        try {
            Drawable drawable = AppCompatResources.getDrawable(context, drawableResource);
            if (drawable != null) {
                Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                byte[] bitmapData = byteArrayOutputStream.toByteArray();
                Image image =  Image.getInstance(bitmapData);
                image.scaleToFit(document.getPageSize().getWidth(), document.getPageSize().getHeight());
                image.setAbsolutePosition(0F, 0F);
                addImage(image);
            }
        } catch (IOException | BadElementException e) {
            Log.e("Error in addImageDrawableAsBackground: ", e.toString());
        }

    }

    //crear tabla y añadirla al documento
    public void createTable(String[] arrayHeaders, ArrayList<String[]> listRowsData) {
        //recordar todos los elementos se añaden como párrafos
        paragraph = new Paragraph();
        //defino tipo de letra a utilizar
        paragraph.setFont(FONT_TEXT);
        //creo tabla pásandole el número de columnas, es decir, el tamaño del array con los encabezados
        PdfPTable pdfPTable = new PdfPTable(arrayHeaders.length);
        //indicamos el ancho de la tabla pasándolo como porcentaje
        pdfPTable.setWidthPercentage(100);
        //Es momneto de añadir las celdas de la tabla. Par ello se define declara un objeto de tipo
        //PdfCell en el que iremos colocando el dato y lo añadiremos a la tabla
        PdfPCell pdfPCell;
        //Primero creo los encabezados de las columnas recorriendo el array de headers e instanciando
        //el objeto pdfPCell con el texto header
        int indexCol = 0;
        while (indexCol < arrayHeaders.length) {
            //creo una celda la frase o contenido de tip Phrase que pide el encabezado y su tipo de letra
            pdfPCell = new PdfPCell(new Phrase(arrayHeaders[indexCol], FONT_SUBTITLE));
            //Indico como quedará alineado horizontalmente el elemento pdfPCell creado
            pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfPCell.setBackgroundColor(BaseColor.GREEN);
            pdfPTable.addCell(pdfPCell);
            indexCol++;
        }
        //Toca ahora crear cada una de las filas almacenadas en la lista
        for (int indexRow = 0; indexRow < listRowsData.size(); indexRow++) {
            String[] arrayRowData = listRowsData.get(indexRow);
            for (indexCol = 0; indexCol < arrayRowData.length; indexCol++) { //puedo reutilizar indexCol
                pdfPCell = new PdfPCell(new Phrase(arrayRowData[indexCol], FONT_TEXT));
                pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                pdfPCell.setFixedHeight(40);
                pdfPTable.addCell(pdfPCell);
            }
        }
        //creada toda la tabla añidimos la tabla al párrafo
        paragraph.add(pdfPTable);
        try {
            document.add(paragraph);
        } catch (DocumentException e) {
            Log.e("Error in createTable: ", e.toString());
        }
    }

    public void addNewPage() {
        document.newPage();
    }

    public void attachPdfAndSendMail() {
        final String[] addresses = {"sat@atlasromero.com", "administracion@atlasromero.com", "laguipemo@me.com"};
        final String subject = "Informe del mantenimiento";
        String textoEmail = "Le adjuntamos el informe del mantenimiento realizado a su vitrina";
        Intent intentEmail = new Intent(Intent.ACTION_SENDTO);
        intentEmail.setData(Uri.parse("mailto:"));
        intentEmail.putExtra(Intent.EXTRA_EMAIL, addresses);
        intentEmail.putExtra(Intent.EXTRA_SUBJECT, subject);
        intentEmail.putExtra(Intent.EXTRA_TEXT, textoEmail);
        Uri uriPdf = Uri.fromFile(pdfFile);
        intentEmail.putExtra(Intent.EXTRA_STREAM, uriPdf);
        if (intentEmail.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intentEmail);
        }
    }

    public void viewPdf() {
        Intent intentViewPDF = new Intent(context, ViewPdfActivity.class);
        intentViewPDF.putExtra("PATH", pdfFile.getAbsolutePath());
        intentViewPDF.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intentViewPDF);
    }
}

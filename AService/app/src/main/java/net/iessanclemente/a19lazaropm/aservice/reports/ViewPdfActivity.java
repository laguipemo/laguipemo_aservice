package net.iessanclemente.a19lazaropm.aservice.reports;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.github.barteksc.pdfviewer.PDFView;

import net.iessanclemente.a19lazaropm.aservice.R;

import java.io.File;

public class ViewPdfActivity extends AppCompatActivity {
    private static final int RESULT_SHOW_PROBLEM = 666;
    private ActionBar actionBar;
    private PDFView pdfView;
    private File filePdf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pdf);

        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_ios_24);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        init();
    }

    private void init() {
        pdfView = findViewById(R.id.viewerPdfPDFView);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            filePdf = new File(bundle.getString("PATH", null));
        }
        if (filePdf != null && filePdf.isFile()) {
            pdfView.fromFile(filePdf)
                    .enableSwipe(true) //cambiar páginas del pdf haciendo swipe
                    .swipeHorizontal(false) // el swipe será veritical (por ello horizontal false)
                    .enableDoubletap(true) // permite hacer zoom con doble tap
                    .enableAntialiasing(true) // mejora visualización en pantallas de baja resolución
                    .spacing(5)
                    .load(); // carga el pdf
        } else {
            sendResult(filePdf, RESULT_SHOW_PROBLEM);
        }
    }

    private void sendResult(File filePdf, int result) {
        Intent intent = new Intent();
        intent.putExtra("FILE", filePdf);
        setResult(result, intent);
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }
}
# A-Service

![logo](doc/img/aservice_logo.png)

**A-Service** es una aplicación desarrollada para dispositivos móviles con sistema operativo Android y enfocada a facilitar el procedimiento seguido en el mantenimiento de vitrinas de extracción de gases.

## Descripción

**Atlas Romero S.L.U.** es una empresa con disposición para la innovación que, entre otras líneas, realizan la instalación completa de sistemas de extracción de gases requeridos en laboratorios de investigación y producción. Para asegurar la correcta evacuación y eliminación de emisiones durante pruebas y ensayos, es necesario **la calibración y el mantenimiento periódico** de estos equipos, certificando que cumplen con los estándares establecidos.

Hasta el momento, todas las guías, formularios e informes relacionados con estos procedimientos son impresos en papel, con todos los inconvenientes que ello implica.

El **propósito** del siguiente proyecto es desarrollar una aplicación móvil que pueda **utilizarse como sustituto a las guías y formularios** actuales y permita además la **digitalización inmediata de la información** así como la **generación automática de informes**.

## Instalación / Puesta en marcha

La aplicación puede ser ejecutada en cualquier dispositivo móvil o tableta, que cuente con una versión del sistema operativo **Android 8.0 Oreo** o superior. En principio, como constituye un software bastante específico y enfocado a una empresa concreta, su instalación y puesta en marcha se llevaría a cabo mediante:

* Descarga del [APK más reciente](distribución/A-Service.apk).
* Transferencia directa al dispositivo.

En el repositorio [Bitbucket](https://bitbucket.org/laguipemo/laguipemo_aservice/src/master/) se pueden encontrar las fuentes de descarga. 

Una vez descargada y/o transferido  el apk al dispositivo, la instalación se puede realizar por medio del Package Installer. También se puede conectar el dispositivo directamente a un ordenador y realizar la instalación desde la terminal:

`adb install <ruta hasta el apk>`

Independientemente de la vía utilizada para su instalación, la primera vez que se ejecuta la app, esta generará una la base de datos completamente funcional, que estará almacenada en la carpeta privada de la aplicación. De modo que los datos están seguros porque, de forma predeterminada, esta área no es accesible para otros usuarios y ni apps. 

## Uso

En todo momento se ha buscado que **A-Service** tenga un a interfaz de usuario limpia, sin distracciones e intuitiva, pero sin menoscavar en su apariencia. 

Después de la pantalla de acceso (login) de rigor, la app tiene como punto de partida una pantalla que muestra:

* Un **listado de las empresas** que Atlas Romero S.L.U. tiene como clientes. Cada elemento de la lista es una tarjeta que muestra información general de la empresa.
* Un botón para **adicionar una nueva empresa** cubriendo el formulario de alta correspondiente.
* Un botón en el extremo derecho de la barra de la app para **cerrar** la aplicación.

Cada **empresa** cuenta con **vitrinas** y a su vez, a cada uno de estos equipos se le realizan **mantenimientos**. Las listas de estos elementos son en forma de tarjeta que muestran información general de ellos. 

La aplicación permite **añadir**, **eliminar** y **editar**:

* Empresas.
* Vitrinas.
* Mantenimientos.

Por otra parte, a la información completa de cada uno de estos elementos se accede con un tab sobre su tarjeta. 

Para la adición de **nuevos elementos** la interfez incorpora el botón correspondiente y para cada caso muestra el formulario a cubrir con los datos requeridos. 

A las opciones para **borrar** o **editar** un elemento se accede mediante el menú contextual. Solo basta con al presionar prologandamente (Long Clic) sobre la tarjeta del elemento en cuestión. 

En el caso de los **mantenimientos**, la edición solo puede realizarla un usuario con privilegios de supervisor. 

Por otra pare la aplicación permite generar un **informe detallado** de todas las **evaluaciones** y **procedimientos** ralizados durante el mantenimiento. Para mostrar dicho **reporte en forma de pdf** y/o  enviarlo por **correo electrónico** a la empresa propietaria del equipo, **A-Service** incorpora sendas opciones en el menú contextual de los mantenimientos.


## Sobre el autor

Mi nombre es Lázaro Guillermo Pérez Montoto me licencié en Química en la Universidad de La Habana, UH (Cuba, 1991). Luego obtuve el titulo de Doctor en Química Orgánica por la Universidad de Santiago de Compostela, USC (2009). Siempre me apasionó la programación y la informática, razón por la cual para mi tesis doctoral desarrollé diferentes Scripts y dos aplicaciones de escritorio en Python para automatizar varias tareas Bioinformáticas en los estudios QSAR (Quantitative Structure-Activity Relationship). 

Lo anterior me valió para ser contratado por el Departamento de Microbiología de la USC para trabajar en el desarrollo de un software para la identificación de Epítopos B Lineales a partir de la estructura primaria de proteínas.

En 2020 obtuve el Certificado de profesionalidad de Nivel 3 “IFCD0210 Desenvolvemento de Aplicacións con Tecnoloxías Web” (628 horas).

Con este proyecto terminaría el ciclo superior de DAM a distancia.

**email:** <laguipemo@gmail.com>

## Licencia


[GNU GENERAL PUBLIC LICENSE Version 3](LICENSE.md)

Los íconos utilizados en la app son suministrados por el sistema operativo o son íconos libres descargados desde [https://icon-icons.com]( https://icon-icons.com) bajo los siguients [términos de uso](https://icon-icons.com/es//terms-of-use/).
En el caso del ícono de la aplicación, logos e imágenes de los reportes pdf generados, son autoría de Rafael Romero Bello propietario de [Atlas Romero S.L.U.](https://www.atlasromero.com/), que a su vez es partner certificado de instalación de [Köttermann GmbH](https://www.koettermann.com/de/).

## Índice

En este proyecto se va a realizar un entregable, polo que se omite la parte de Análisis y de Planificación. Debido a su extensión, dentro del apartado de Implantación se ha documentado la fase de despliegue y el manual del usuario en documentos independientes.

1. Anteproyecto
    * 1.1. [Idea](doc/templates/1_idea.md)
    * 1.2. [Necesidades](doc/templates/2_necesidades.md)
2. [Diseño](doc/templates/5_deseño.md)
3. [Implantación](doc/templates/6_implantacion.md)
4. [Manual de usuario](doc/templates/manual_usuario.md)


## Guía de contribución

Este proyecto, aunque bastante funcional, es apenas un prototipo que puede recibir todo tipo de contribución y mejoras. Siempre que se distribuyan bajo las mismas condi iones.

Por ejemplo por cuestiones de tiempo no se pudo implementar:

* La incorporación de fotos tomadas con el dispositivo durante los mantenimientos para enriquecer, su documentación y su reporte final.
* Implementación de la base de datos en un servior, para compartirla entre los dos o tres dispositivos de la empresa.
* Incorporar la geración de factura simplificada del mantenimiento.
* Mejoras y optimización de la interfaz.
* Depuración de errors y/o vulnerabilidades.

## Links

A modo de orientación a personas interesadas en los elementos utilizados en el proyecto, a continución se presenta una realación de documentaciones y sitos consultados:

* [Cómo guardar datos con SQLite](https://developer.android.com/training/data-storage/sqlite).
* [Descripción general del almacenamiento de archivos y datos](https://developer.android.com/training/data-storage#filesInternal).
* [Cómo obtener un resultado de una actividad](https://developer.android.com/training/basics/intents/result).
* [iText Examples](https://kb.itextpdf.com/home/it7kb/examples?id=104).
* [Cómo iniciar una actividad con una animación ](https://developer.android.com/training/transitions/start-activity).

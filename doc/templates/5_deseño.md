# FASE DE DESEÑO

## Modelo conceptual do dominio da aplicación e/ou Diagrama de clases [usando UML, ConML, ou linguaxe semellante].

Después de la pantalla "Splash" y la de acceso (Login), la aplicación comienza con la clase `ListEmpresasActivity` ubicada en el paquete **`main`**. Esta clase muestra la lista de las empresas clientes por medio de elementos de tipo CardView. De igual modo, las clases que muestran la información de una empresa `FichaEmpresaActivity` y la información de una vitrina `FichaVitrinaActivity` incluidas en el paquete **`ui.secondary`**, también muestran listas de elementos de tipo CardView. En específico, muestran la relación de vitrinas de una empresa y la relación de mantenimientos de una vitrina respectivamente. En todas estas listas, la información mostrada en los elementos CardView, es manejada por su correspondiente adaptador, clase que hereda `RecyclerView.Adapter`, agrupadas todas ellas en el paquete **`adapters`**.

<div align="center">
  <img src="../img/UML_Class_Adapter-Ui.png" >
</div><br>

Por otra parte, en el paquete **`ui.forms`** se encuentran organizadas las interfaces visuales a través de las cuales el usuario introduce los datos para procesos de inserción o edita los datos para proceso de actualización sobre la base de datos.

Las clases u objetos en los que se transfiere la información entre las interfaces gráficas y la base de datos, se encuentran en reunidas en el paquete **`database.dto`**.

Mientras que las clases responsables de manejar el acceso a los datos:

* `DataBaseContract`: contiene la información de los nombres de los campos de cada tabla de la base de datos. La información está encapsulada en clases estáticas, una para cada tabla.
* `DataBaseHelper`: clase que hereda de `SQLiteOpenHelper` y es la encargada de abrir la conexión con la base de datos o crear una nueva en caso que no exista. Es en ella donde se garantiza la carga de datos iniciales durante el primer arranque. toda la estructura de una nueva en caso de que no exista.
* `DataBaseOperations`: clase auxiliar con patrón singleton en la que se implementan las operaciones CRUD sobre las entidades definidas en la base de datos.
todas ellas están recopiladas en el paquete **`database.dao`**.

<div align="center">
  <img src="../img/UML_Class_Forms-database.png" >
</div><br>

Por otra parte, en el paquete **`ui.exta`** también tenemos una interfaz gráfica (`MedicionesVolumenExtraccion`) creada para que el usuario introduzca las mediciones necesarias para el cálculo del volumen de extracción y se transmitan a la base de datos haciendo uso de su dto.

Para generar y visulaizar el informe o reporte final se cuenta con las clases incluidas den el paquete **`reports`**. Básicamente son una clase que funge como plantilla (`TemplatePdf`) con la estructura del informe y a la se le van pasando los datos a mostrar en cada caso y la otra clase `ViewPdfActivity` con la que logramos visualizar el pdf.

<div align="center">
  <img src="../img/UML_Class_Reports.png" >
</div><br>

## Casos de uso [descritos en fichas e/ou mediante esquemas; deben incluír o(s) tipo(s) de usuario implicados en cada caso de uso].

Para los casos de uso se ha contemplado dos actores, un usuario general (Técnico) y una particularización de este, como es un usuario de tipo Supervisor. Este último puede realizar todas las acciones de un Técnico y además modificar o actualizar datos de un mantenimiento:

<div align="center">
  <img src="../img/UML_Uses_Cases.png" >
</div><br>

## Deseño de interface de usuarios [mockups ou diagramas...].

A continuación se muestran capturas de pantalla de las diferentes interfaces de usuario por las se transita durante el uso de **A-Service**.

En las dos primeras imágenes se aprecian la pantalla splash y la de inicio y, una vez dentro de la app, la pantalla principal con las lista de empresas clientes, en forma de tarjetas. También en esta imagen se muestra la captura correspondiente al formulario a cubrir al añadir una empresa nueva (clic en el botón de añadir), así como una muestra del menú contextual al mantener pulsada una empresa. Menú que nos ofrece las opciones de eliminar o de editar. En este último caso, nos muestra el formulario pero esta vez editable para hacer los cambios deseados:

<div align="center">
  <img src="../img/CapturasInterfacesUsuario_01.png" >
</div><br>

Por otra parte si seleccionamos una empresa, accedemos a su ficha en la que se muestra al usuario los datos generales de la empresa y una lista de tarjetas con las vitrinas instaladas en dicha empresa (primera captura). Nuevamente se cuenta con un botón para la adición de una vitrina nueva y para ello se cubriría el formulario mostrados en la 2 y 3ra captura. Aunque no se muestra en estas imágenes, se puede realizar la eliminación de una vitrina y/o la edición de sus datos a través un menú contextual idéntico al de las empresas. Esta última acción llevaría al formulario mostrando los datos a editar.
Si seleccionamos una vitrina, entonces accedemos a su ficha, que muestra
su información general y la relación de mantenimientos que se le ha realizado. Nuevamente se cuenta con un botón para la adición de un nuevo mantenimiento. Para ello se seguiría un formulario mucho más amplio que va cubriendo todas las evaluaciones establecidas en los protocolos (resto de capturas):

<div align="center">
  <img src="../img/CapturasInterfacesUsuario_02.png" >
</div><br>

Ejemplos de las insterfaces de usuario y sus componentes fundamentales, selector de fecha, autocompletable para entrada del nombre del técnico, que a partir del segundo carácter introducido, comienza a mostrar las coincidencias con los técnicos existentes en la base de datos, típicos checkbox, desplegable (spinners) con los valores posibles para evaluaciones cualitativas, etc. El caso del volumen de extracción, se requieren 9 mediciones para calcularlo y para ello y una vez hecho el cálculo, si el resultado se sale del rango normal, su etiqueta se muestra en rojo y el spinner de evaluación se fija en Requiere Reparación (R.R.). Si se selecciona un mantenimiento, entonces se mostrarían los resultados obtenidos cuando se realizó:

<div align="center">
  <img src="../img/CapturasInterfacesUsuario_03.png" >
</div><br>

En el caso de los mantenimientos, el menú contextual además de añade dos nuevas opciones (comparado con el de empresas y de vitrinas). Una permite generar y mostrar el informe del mantenimiento en formato pdf y la otra, luego de generarlo, permite su envío como fichero adjunto: 

<div align="center">
  <img src="../img/CapturasInterfacesUsuario_04.png" >
</div><br>

En este punto hay que destacar que, la edición de mantenimiento está restringida para supervisores por lo que se solicita el pin de supervisor.

## Diagrama de Base de Datos.

En el caso de la persistencia de datos haremos uso de **SQLite**, que, más que un Sistema Gestor de Base de Datos (**SGBD**), es una biblioteca escrita en C que implementa un **SGBD** y que permite transacciones sin necesidad de un servidor ni configuraciones. Además es open source y sus principales características:

* Pequeño tamaño (al ser una biblioteca, es mucho menor que cualquier **SGDB**).
* Reunir los cuatro criterios **ACID** (Atomicidad, Aislamiento y Durabilidad), aportando gran estabilidad.
* Gran **portabilidad** y **rendimiento**.
la convirtieron en el candidato ideal para este proyecto. Teniendo en cuenta además, que su principal desventaja que es la escalabilidad, no nos afectaría dada la envergadura del proyecto.

## Diagrama Entidad-Relación

Las entidades de la base de datos diseñada para la app se muestra en el siguiente diagrama:

<div align="center">
  <img src="../img/AtlasERD_Dia.png" >
</div><br>


## Diagrama de compoñentes software que constitúen o produto e de despregue.

### Diagrama de despliegue

<div align="center">
  <img src="../img/Diagrama_despliegue.png" >
</div><br>


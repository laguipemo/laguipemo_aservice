# Manual de Usuario

<div align="left">
  <img src="../img/aservice_logo.png" />
</div>

A-Service versión Beta.

## **Indice**

1.[Inicio Sesión](#inicio_sesion)

&nbsp;&nbsp;&nbsp;&nbsp; 1.1.[¿Olvidó su contraseña?](#olvido_contraseña)

2.[Empresas](#empresas)

&nbsp;&nbsp;&nbsp;&nbsp; 2.1.[Añadir nueva empresa](#nueva_empresa)

&nbsp;&nbsp;&nbsp;&nbsp; 2.2.[Eliminar empresa](#eliminar_empresa)

&nbsp;&nbsp;&nbsp;&nbsp; 2.3.[Editar empresa](#edit_empresa)

3.[Vitrinas](#vitrinas)

&nbsp;&nbsp;&nbsp;&nbsp; 3.1.[Añadir nueva vitrina](#nueva_vitrina)

&nbsp;&nbsp;&nbsp;&nbsp; 3.2.[Eliminar vitrina](#eliminar_vitrina)

&nbsp;&nbsp;&nbsp;&nbsp; 3.3.[Editar vitrina](#edit_vitrina)

4.[Mantenimientos](#mantenimientos)
   
&nbsp;&nbsp;&nbsp;&nbsp; 4.1.[Añadir nuevo mantenimiento](#nuevo_mantenimiento)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - [Calcular volumen de extracción](#mediciones)

&nbsp;&nbsp;&nbsp;&nbsp; 4.2.[Eliminar mantenimiento](#eliminar_mantenimiento)

&nbsp;&nbsp;&nbsp;&nbsp; 4.3.[Editar mantenimiento](#edit_mantenimiento)

&nbsp;&nbsp;&nbsp;&nbsp; 4.4.[Mostrar informe](#ver_informe)

&nbsp;&nbsp;&nbsp;&nbsp; 4.5.[Enviar informe por email](#enviar_informe)


#

<a name="inicio_sesion"></a>
## **1. Inicio de Sesión**

La pantalla de inicio impide el uso de la app por parte de personas ajenas a la empresa y/o personas no autorizadas a utilizarla. En ella se solicita el usuario y la contraseña con la que el supervisor o responsable principal de los mantenimientos en la empresa lo registró. Después de introducir los datos se presiona el botón inicio para comenzar:

<div align="center">
  <img src="../img/Captura_01.png" />
</div>

<a name="olvido_contraseña"></a>
### &nbsp;&nbsp;&nbsp;&nbsp; 1.1 ¿Olvidó su contraseña?

<p style="margin-left: 35px">En caso de olvidar la contraseña, seleccione <b>¿Olvidó su clave?</b>. Se le solicitarán sus datos y se le enviará un correo al supervisor para que conozca la incidencia y se ponga en contacto con Ud.</p>

<div align="center">
  <img src="../img/Captura_02.png" />
</div>

<a name="empresas"></a>
## **2. Empresas**

Al iniciar sesión en **A-Service** se muestran la lista de la empresas clientes en forma de tarjetas con su información básica:

<div align="center">
  <img src="../img/Captura_03.png" />
</div>

Desde esta pantalla podemos:

<a name="nueva_empresa"></a>
### &nbsp;&nbsp;&nbsp;&nbsp; 2.1 Añadir nueva empresa

<p style="margin-left: 35px">Para añadir una nueva empresa basta con presionar el botón <img src="../img/boton_01.png"/> y cubrir el formulario para nueva empresa:</p>

<div align="center">
  <img src="../img/Captura_04.png" />
</div>

<a name="eliminar_empresa"></a>
### &nbsp;&nbsp;&nbsp;&nbsp; 2.2 Eliminar empresa

<p style="margin-left:35px">Para eliminar una empresa se mantiene presionada su tarjeta hasta que aparezca el menú contextual y se selecciona la opción <img src="../img/boton_02.png" /><b>Eliminar</b>:</p>

<div align="center">
  <img src="../img/Captura_05.png" />
</div>

<a name="edit_empresa"></a>
### &nbsp;&nbsp;&nbsp;&nbsp; 2.3 Editar empresa

<p style="margin-left:35px">Para editar la información general sobre una empresa, se mantiene presionada su tarjeta hasta que aparezca el menú contextual y se selecciona la opción <img src="../img/boton_03.png" /><b>Actualizar</b>.</p>

<p style="margin-left:35px">En este caso se muestra el formulario con los datos de la empresa seleccionada para que Ud realice los cambios deseados.</p>

<div align="center">
  <img src="../img/Captura_06.png" />
</div>

<a name="vitrinas"></a>
## **3. Vitrinas**

En la pantalla que representa la ficha de una empresa determinada, además de la información general de la empresa, también se muestra la lista de sus vitrinas en forma de tarjetas. Para acceder a sus datos pues basta con seleccionarla (Tab) y se accede a su ficha y desde ella se puede:

<div align="center">
  <img src="../img/Captura_07_0.png" />
</div>

<a name="nueva_vitrina"></a>
### &nbsp;&nbsp;&nbsp;&nbsp; 3.1 Añadir nueva vitrina

<p style="margin-left:35px">Para añadir una nueva vitrina basta con presionar el botón <img src="../img/boton_01.png" /> y cubrir el formulario para nueva vitrina:</p>

<div align="center">
  <img src="../img/Captura_07_1.png" />
</div>

<a name="eliminar_vitrina"></a>
### &nbsp;&nbsp;&nbsp;&nbsp; 3.2 Eliminar vitrina

<p style="margin-left:35px">Para eliminar una vitrina se mantiene presionada su tarjeta hasta que aparezca el menú contextual y se selecciona la opción <img src="../img/boton_02.png" /><b>Eliminar</b>:</p>

<div align="center">
  <img src="../img/Captura_08.png" />
</div>

<a name="edit_vitrina"></a>
### &nbsp;&nbsp;&nbsp;&nbsp; 3.3 Editar vitrina

<p style="margin-left:35px">Para editar la información general sobre una vitrina, se mantiene presionada su tarjeta hasta que aparezca el menú contextual y se selecciona la opción <img src="../img/boton_03.png" /><b>Actualizar</b>.</p>

<p style="margin-left:35px">En este caso se muestra el formulario con los datos de la vitrina seleccionada para que Ud realice los cambios deseados.</p>

<div align="center">
  <img src="../img/Captura_09.png" />
</div>

<a name="mantenimientos"></a>
## **4. Mantenimientos**

En la pantalla correspondiente a una vitrina, se muestra su información general y la lista de mantenimientos que se les ha realizado. Al seleccionar un mantenimiento se pasa entonces a su ficha des la que se puede realizar las siguientes acciones:

<div align="center">
  <img src="../img/Captura_10.png" />
</div>

<a name="nuevo_mantenimiento"></a>
### &nbsp;&nbsp;&nbsp;&nbsp; 4.1 Añadir nuevo mantenimiento

<p style="margin-left:35px">Para añadir una nuevo mantenimiento basta con presionar el botón <img src="../img/boton_01.png" /> y cubrir el formulario para nuevo mantenimiento:</p>

<div align="center">
  <img src="../img/Captura_11.png" />
</div>

<p style="margin-left:35px">dentro de este formulario hay un parámetro, <b>Volumen de Extracción</b>, que requiere ser calculado.</p>

<a name="mediciones"></a>
<ul>
  <li style="margin-left:50px; font-weight: bold">Calcular volumen de extracción<</li>
</ul>
<p style="margin-left:50px">Para poder efectuar este cálculo se necesitan realizar 9 mediciones del flujo de gases en frontal y calcular su media. Es por ello que al seleccionar su EditText, se abre el formulario correspondiente:</p>

<div align="center">
  <img src="../img/Captura_12.png" />
</div>

<p style="margin-left:50px">en el caso de que el valor calculado se salga del intervalo permitido, la etiqueta de este parámetro se muestra en color rojo y el Spinner con su evaluación cualitativa se bloquea en el valor Requiere Reparación (R.R.).</p>

<a name="eliminar_mantenimiento"></a>
### &nbsp;&nbsp;&nbsp;&nbsp; 4.2 Eliminar mantenimiento

<p style="margin-left:35px">Para eliminar un mantenimiento se mantiene presionada su tarjeta hasta que aparezca el menú contextual y se selecciona la opción <img src="../img/boton_02.png" /><b>Eliminar</b>:</p>

<div align="center">
  <img src="../img/Captura_13.png" />
</div>

<a name="edit_mantenimiento"></a>
### &nbsp;&nbsp;&nbsp;&nbsp; 4.3 Editar mantenimiento

<p style="margin-left:35px">Para editar la información general sobre un mantenimiento, se mantiene presionada su tarjeta hasta que aparezca el menú contextual y se selecciona la opción <img src="../img/boton_03.png" /> <b>Actualizar</b>. Para esta acción se requieren permisos de <b>Supervisor</b> por lo que aparece un diálogo solicitando el correspondiente pin para continuar.</p>

<div align="center">
  <img src="../img/Captura_13.png" />
</div>

<p style="margin-left:35px">En caso de tener los permisos para editar mantenimientos y haber introducido el pin correcto, se muestra el formulario con los datos de del mantenimiento seleccionado para que Ud realice los cambios deseados.</p>

<div align="right">
  <img width="700px"; src="../img/CapturasInterfacesUsuario_03.png" />
</div>

<a name="ver_informe"></a>
### &nbsp;&nbsp;&nbsp;&nbsp; 4.4 Mostrar informe en pdf

<p style="margin-left:35px">Si se desea generar y ver el informe o reporte en pdf del mantenimiento seleccionado, basta con acceder a la a través del menú contextual, a la opción <img src="../img/boton_04.png" /> <b>Mostrar reporte pdf</b>. Entonces se mostrará el informe de 3 páginas, con el aspecto personalizado de <b>Atlas Romero S.L.U.</b> :</p>

<div align="right">
  <img width="700px"; src="../img/Captura_14.png" />
</div>

<a name="enviar_informe"></a>
### &nbsp;&nbsp;&nbsp;&nbsp; 4.5 Enviar informe por email

<p style="margin-left:35px">Si se desea generar y enviar el informe o reporte por email, basta con acceder a la a través del menú contextual, a la opción <img src="../img/boton_05.png" /> <b>Enviar reporte pdf</b>. Entonces abre la aplicación de correo electrónico del dispositivo con un mensaje con el reporte adjunto.</p>

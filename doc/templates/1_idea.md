# Idea

## Tipo de proyecto

El proyecto consiste en una aplicación Adroid entregable, que pueda ser ejecutada en dispositivos móviles y tablets.

## Propósito principal

Atlas Romero S.L.U. es una empresa con disposición para la innovación que entre otras líneas, realizan la instalación completa de sistemas de extracción de gases requeridos en laboratorios, para asegurar la evacuación y eliminación de emisiones de gases durante pruebas y ensayos.

El propósito del siguiente proyecto es desarrollar una aplicación móvil que pueda utilizarse en procesos de evaluación, validación y calibrado de estos equipos.

## Destinatarios

Los destinatarios de la aplicación serán los ingenieros y técnicos de la empresa Atlas Romero S.L.U., encargados de llevar a cabo los procedimientos de validación, calibrado, reparación, mantenimiento y/o control de calidad estipulados para los sistemas de extracción de gases de sus clientes.

## Alcance

Hasta el momento todas las guías empleadas y formularios a cumplimentar en los procesos mencionados son impresos en papel, con todos los inconvenientes que ello implica. La aplicación que se desarrollará permitirá la digitalización y cierta automatización de estas actividades. Al ser, en primera instancia, un software muy específico no conocemos la existencia de ningún otro en este contexto.

## Requisitos básicos

Teniendo en cuenta la incomodidad, el impacto ambiental de la utilización de procedimientos y formularios de papel, unido a las ventajas de una digitalización de la actividad, la aplicación deberá ser capaz de realizar las siguientes acciones básicas:

* Seleccionar una empresa existente en la base de datos de clientes o crear una nueva en caso de que no exista.
* Mostrar los procedimientos asociados a las instalaciones y equipos de la empresa cliente seleccionada.
* Seleccionar el procedimiento que se realizará o crear uno nuevo añadiendo los pasos necesarios en concordancia con las necesidades del cliente.
* Mostrar los pasos del procedimiento seleccionado que, además de ser la guía a seguir, conforman el formulario que recogerá los valores de mediciones y/o resultados de las comprobaciones que correspondan a cada paso.
* Generar un informe final a partir de los datos recogidos en el formulario del procedimiento realizado.
* Gestión de usuarios, empresas clientes, procedimientos, informes, etc.

## Tecnologías a emplear

Para la implantación de de este software necesitaremos un dispositivo Android (Tablet o Móvil) que tenga como mínimo la versión 8.0 del sistema operativo. Todos los datos necesarios para la creación de empresas, procedimientos, pasos, etc serán dinámicos y se obtendrán de la interacción con la correspondiente base de datos. 

En específico se utilizará SQLite que, más que un Sistema Gestor de Base de Datos (SGBD), es una biblioteca escrita en C que implementa un SGBD y que permite transacciones sin necesidad de un servidor ni configuraciones. Además es Open Source y sus principales características:

* Pequeño tamaño (al ser una biblioteca, es mucho menor que cualquier SGDB).
* Reunir los cuatro criterios ACID (Atomicidad, Aislamiento y Durabilidad), aportando gran estabilidad.
* Gran portabilidad y rendimiento.
la convirtieron en el candidato ideal para este proyecto. Teniendo en cuenta además, que su principal desventaja que es la escalabilidad, no nos afectaría dada la envergadura del proyecto.

## Aspectos por determinar y mejoras de futuro

Un punto muy importante a tener en cuenta de cara a mejoras futuras es la ubicación de la BBDD en un servidor. Además a efectos prácticos, resulta interesante que la app pudiera incorporar fotos del trabajo realizado para enriquecer el informe y que sea capaz de generar una factura simplificada.
# FASE DE IMPLANTACIÓN

## Manual técnico:

### Requisitos 

El hardware requerido para poder ejecutar **A-Service** es cualquier dispositivo móvil o tableta, que cuente con una versión del sistema operativo **Android 8.0 Oreo** (nivel de API 26) o superior. Este requisito es satisfecho actualmente por el 82.7% de los dispositivos aproximadamente.

Por otra parte pueden ser requeridos permisos para de lectura y escritura de datos en el almacenamiento interno y hará uso de una aplicación de correo externa para el envío de los reportes generados.

## Instalación

Aunque lo más deseable resulta ser que la app pueda ser descargada desde **GooglePlay**, en un principio, como constituye un software bastante específico y enfocado a una empresa concreta, su instalación y puesta en marcha se llevaría a cabo mediante:

* Descarga del [APK más reciente](../../distribución/A-Service.apk).
* Transferencia directa al dispositivo.

En el repositorio [Bitbucket](https://bitbucket.org/laguipemo/laguipemo_aservice/src/master/distribuci%C3%B3n/) se pueden encontrar las fuentes de descarga. 

Una vez descargada y/o transferido el apk al dispositivo:

<div align="center">
  <img src="../img/CapturasInstalacion_0.png" >
</div><br>

la instalación se puede realizar por medio del Package Installer al ejecutar el apk. La secuencia de pasos durante instalación se muestra a continuación:

<div align="center">
  <img src="../img/CapturasInstalacion_1.png" >
</div><br>

También se puede conectar el dispositivo directamente a un ordenador y realizar la instalación desde la terminal:

`adb install <ruta hasta el apk>`

## Configuración inicial, seguridad, control de usuarios

Independientemente de la vía utilizada para su instalación, la primera vez que se ejecuta la app, esta generará una la base de datos completamente funcional, sin requerirse ningún otro tipo de configuración inicial o adicional

La base de datos estará almacenada en la carpeta interna de la app. De modo que los datos estarán seguros teniendo en cuenta que, de forma predeterminada, esta área no es accesible para otros usuarios y ni apps. 

## Proceso de despliegue

Para poder realizar el despliegue es necesario generar el apk de la aplicación con un certificado válido. Primero generamos el archivo de la apk de la aplicación firmada con un keystore (almacén de claves).

Al generar el apk pro primera vez necesitamos crear un nuevo almacén de claves:

<div align="center">
  <img src="../img/Generate_apk_01-NewKeyStore.png" >
</div><br>

Una vez creado, generamos entonces el apk firmado con la nueva key generada:

<div align="center">
  <img src="../img/Generate_apk_02-SignedBundleOrApk.png" >
</div><br>

siempre que compilemos una nueva versión, es necesario firmarla con el mismo certificado para que el S.O Android la interprete como una actualización.

Luego se hace la modificación a la versión compilada de **debug** a **release**:

<div align="center">
  <img src="../img/Generate_apk_03-SignedBundleOrApkRelease.png" >
</div><br>

Finalmente tenemos el apk-release listo para pasarlo al dispositivo donde se instalará:

<div align="center">
  <img src="../img/Generate_apk_04-AppRelease_apk_created.png" >
</div><br>

Apk pasado al dispositivo para su instalación:

<div align="center">
  <img src="../img/Generate_apk_04-AppRelease_apk_created.png" >
</div><br>

### Información relativa á administración do sistema, é dicir, tarefas que se deberán realizar unha vez que o sistema estea funcionando, como por exemplo:

En principio no se requiere realizar ninguna tarea adicional o especial una vez que el sistema se encuentre funcionando. De todos modo, no está demás recordar la realización de copias de seguridad (backup) periódicamente. Para esta primera versión de la aplicación, este procedimiento se realizar manualmente conectado el dispositivo a un ordenador.
Cabe destacar que, este es un punto previsto como mejora futura: externalizar la BBDD para compartir entre varios dispositivos y centralizar copias y otras acciones relacionadas con ella.

## Protección de datos de carácter persoal.

Los datos personales de técnicos, empresas clientes y su personal de contacto son de absoluta responsabilidad de la empresa explota la aplicación. En este caso Atlas Romero tiene sus políticas para el [cumplimiento del Reglamento General de Protección de Datos (RGPD)](../../LICENSE.md).
A-Service por su parte no recopila ninguna otra información y no hace un tratamiento especial o adicional sobre ellos.

## Manual de usuario

[Ir al Manual de Usuario](../templates/manual_usuario.md)

# ESTUDO DE NECESIDADES E MODELO DE NEGOCIO

## Xustificación das necesidades detectadas que cubre o sistema a desenvolver

Describe o problema ou a necesidade.

**Atlas Romero S.L.U.** es una empresa con disposición para la innovación que entre otras líneas, realizan la instalación completa de sistemas de extracción de gases requeridos en laboratorios para asegurar la evacuación y eliminación de emisiones durante pruebas y ensayos.
En la actualidad todas las guías, protocolos, formularios par la realización de montaje/puesta en marcha, mantenimiento, reparación, calibración de la vitrinas de extracción de gases son impresos en papel. Además, las mediciones, resultados de evaluaciones visuales, etc son pasados posteriormente a plantillas para generar los reportes e informes finales.

1.Por que é necesaria a posta en marcha dun proxecto que aborde dita necesidade?

Teniendo en cuenta las necesidades comentadas anteriormente, resulta evidente las ventajas de una app que permita la digitalización y cierta automatización en el proceso. Por una parte, además de evitar el uso de papel, también pasan a ser inmediatas las acciones de cubrir formularios de mantenimiento y las generación de sus reportes finales.

3.Cal é o obxectivo xeral que persegue o proxecto?

El propósito del siguiente proyecto es desarrollar una aplicación móvil que pueda utilizarse en procesos de montaje/puesta en marcha y mantenimientos de Vitrinas de extracción de gases.

4.Responde a estas preguntas concretas:

4.1.Como se pode responder a esta necesidade?

Digitalizar formularios a cubrir durante los mantenimientos. Organizar dichos formularios para que sean guías de los protocolos de evaluación. Automatizar la generación informes.

4.2. Que pode facerse para cambiar este estado de cousas?

Con una aplicación móvil que integre: los formularios requeridos, la posibilidad de cubrirlos en el instante en que se verifica cada paso de los protocolos de evaluación, la generación y emisión de informes finales.

4.3. Como podemos contribuir desde a nosa situación a que o problema se resolva?

Desde nuestra situación hemos decidió desarrollar una aplicación móvil para el sistema operativo Android, teniendo en cuenta las numerosas ventajas que presenta:

* La enorme diversidad de dispositivos como móviles y tableas que dicho sistema en los que funcionaría nuestra app.
* La gran diversidad de tamaños y precios de dichos dispositivos que permiten una elección más adecuada a las necesidades y posibilidades de la empresa.
* Lo fácil y práctico que resultan este tipo de aplicaciones y dispositivos frente a la utilización de ordenadores portátiles.

4.4. Que medios, actividades e recursos van poñer en xogo?

Para la realización de este proyecto se utilizará Android Studio, debido a que es el Entorno de Desarrollo Integrado (IDE) oficial para trabajar en la plataforma Android. Como lenguaje de programación se utilizará Java empleando como principales librerías y APIs:

* APIs nativas de Google incluidas en el propio IDE. Por ejemplo las APIs para SQLite.

* [iText](https://itextpdf.com/es/resources/api-documentation/itext-5-java). Generación de pdfs.

* [Adroid Pdf Viewer](https://github.com/barteksc/AndroidPdfViewer). Visualización de pdfs.

En cuanto al diseño, dentro de lo posible, se intentará seguir las recomendaciones de Material Design. Los layout serán responsivos buscando su adaptación a los distintos tipos de pantalla. Se intentará en todo momento mantener la internacionalización para Español e Inglés, con futura inclusión de Gallego. Se emplearán CardViews dentro de RecylerViews para mostrar las listas tarjetas de Empresas, Vitrinas y mantenimientos.

4.5. Que actividades se van realizar?

Se pretende:

* Realizar la verificación de usuario y contraseña del técnico a utilizar la app contra los incluidos en la base de datos como autorizados.

* Leer de la base de datos y mostrar la información de la empresa y la lista de empresas clientes para seleccionar con la que se trabajará.

* Adicionar, eliminar o editar empresas de la base de datos.

* Leer de la base de datos los equipos (Vitrinas de extracción de gases) pertenecientes a la empresa seleccionada y mostrar todo su información incluyendo la lista de mantenimientos que se le han practicado.

* Añadir, eliminar o editar vitrinas de la base de datos.

* Leer de la base de datos y mostrar toda la información de un mantenimiento seleccionado.

* Generar para mostrar por pantalla o para enviar adjunto por correo electrónico, el pdf del informe final del mantenimiento seleccionado.

* Añadir y eliminar mantenimiento de la base de datos.

* Comprobar que, en el caso de la edición de un mantenimiento, el usuario sea un supervisor.

4.6. Que metodoloxía se vai empregar para levar a cabo o traballo?

Se intentará, en la medida de lo posible y teniendo en cuenta del escaso tiempo disponible y lo aprendido en el módulo, una separación de la vista de lo que es la lógica de negocio. Por ejemplo el uso de patrón MVP (Modelo-Vista-Presentador).

4.7. Que persoas serían precisas para realizar o proxecto con éxito?

En principio se requerirá de un programador con retroalimentación continua con el responsable de la empresa a la que se destina la app. 

4.8 Con canto tempo se conta? 

Según la guía de desarrollo del PFC la estimación del módulo es de 5 créditos ECTS, que equivalen aproximadamente a 125 horas de trabajo.

4.9 Canto tempo se necesita?

El tiempo total estimado es de 480 horas que se dividen de la siguiente manera:
* 1 UT para análisis, diseño y planificación.

* 4 UT para la entrega del prototipo.

* 1 UT Implantación y desarrollo de la documentación.

Por UT (Unidad de Tiempo) se entienden 4 semanas de 5 días laborables de 4 horas diarias de trabajo.

6 UT x 4 semanas x 5 días x 4 horas = 480 horas

## Posibilidades de comercialización (viabilidade, competidores…).

En un principio no se pretende la comercialización de la app.

